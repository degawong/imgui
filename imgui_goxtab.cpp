#include "imgui_goxtab.h"

#include <imgui.h>

#define IMGUI_DEFINE_MATH_OPERATORS
#include <imgui_internal.h>

namespace ImGui {

bool GoxTab(const char *text, int* selected, int id)
{
    ImFont *font = GImGui->Font;
    ImGuiContext& g = *GImGui;
    const ImGuiStyle& style = g.Style;
    float pad = style.FramePadding.x;
    ImVec2 text_size = CalcTextSize(text);
    ImGuiWindow* window = GetCurrentWindow();
    ImVec2 pos = window->DC.CursorPos + ImVec2(pad, text_size.x + pad);

    const ImU32 text_color = ImGui::ColorConvertFloat4ToU32(style.Colors[ImGuiCol_Text]);
    const ImVec4 color = style.Colors[(*selected != id) ? ImGuiCol_Button : ImGuiCol_ButtonActive];
    ImGui::PushStyleColor(ImGuiCol_Button, color);
    ImGui::PushID(text);
    bool ret = ImGui::Button("", ImVec2(text_size.y + pad * 2, text_size.x + pad * 2));
    ImGui::PopStyleColor();

    char c;
    while ((c = *text++))
    {
        const ImFontGlyph *glyph = font->FindGlyph(c);
        if (!glyph) continue;

        window->DrawList->PrimReserve(6, 4);
        window->DrawList->PrimQuadUV(
            pos + ImVec2(glyph->Y0, -glyph->X0),
            pos + ImVec2(glyph->Y0, -glyph->X1),
            pos + ImVec2(glyph->Y1, -glyph->X1),
            pos + ImVec2(glyph->Y1, -glyph->X0),

            ImVec2(glyph->U0, glyph->V0),
            ImVec2(glyph->U1, glyph->V0),
            ImVec2(glyph->U1, glyph->V1),
            ImVec2(glyph->U0, glyph->V1),
            text_color);
        pos.y -= glyph->AdvanceX;
    }
    ImGui::PopID();

    if (ret)
        *selected = id;

    return ret;
}

} // namespace ImGui
