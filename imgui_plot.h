#pragma once

#include "imgui.h"

namespace ImGui {

void PlotMultiLines(
    const char* label,
    int num_datas,
    const char** names,
    const ImColor* colors,
    float(*getter)(const void* data, int idx),
    const void * const * datas,
    int values_count,
    int values_offset = 0,
    float scale_min = FLT_MAX,
    float scale_max = FLT_MAX,
    ImVec2 graph_size = ImVec2(0, 0));

void PlotMultiHistograms(
    const char* label,
    int num_hists,
    const char** names,
    const ImColor* colors,
    float(*getter)(const void* data, int idx),
    const void * const * datas,
    int values_count,
    int values_offset = 0,
    float scale_min = FLT_MAX,
    float scale_max = FLT_MAX,
    ImVec2 graph_size = ImVec2(0, 0));

} // namespace ImGui
